<?php

namespace Traits;

trait ErrorsBag
{
    /**
     * Errors bag
     * @var array
     */
    private $errors = [];

    /**
     * Add error to the bag
     * @param $attribute
     * @param $message
     */
    public function addError($attribute, $message): void
    {
        $this->errors[$attribute] = $message;
    }

    /**
     * Add errors to the bag
     * @param array $errors
     */
    public function addErrors(array $errors): void
    {
        $this->errors[] = $errors;
    }

    /**
     * Get error
     * @param $attribute
     * @return mixed
     */
    public function getError($attribute)
    {
        return $this->hasError($attribute) ? $this->getError($attribute) : null;
    }

    /**
     * Get errors
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Has error
     * @param $attribute
     * @return bool
     */
    public function hasError($attribute): bool
    {
        return isset($this->errors[$attribute]);
    }

    /**
     * Has errors
     * @return bool
     */
    public function hasErrors(): bool
    {
        return (bool) $this->errors;
    }
}