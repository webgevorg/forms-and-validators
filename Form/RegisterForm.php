<?php

namespace Form;

use Traits\FormValidator;

class RegisterForm extends AbstractForm
{
    use FormValidator;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [];
    }
}