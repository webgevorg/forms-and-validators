<?php

namespace Validator;

use Closure;

interface ValidatorEventInterface
{
    /**
     * After validator success
     * @param Closure $callback
     */
    public function passed(Closure $callback): void;

    /**
     * After validator fail
     * @param Closure $callback
     */
    public function fails(Closure $callback): void;
}