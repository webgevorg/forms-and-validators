<?php

namespace Validator\Rules;

class Required extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public function message($attribute): string
    {
        return "Attribute {$attribute} is required.";
    }

    /**
     * @inheritDoc
     */
    public function validate($value): bool
    {
        return $value !== '' || $value !== null;
    }
}