<?php

namespace Validator\Rules;

class Integer extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public function validate($value): bool
    {
        return is_int($value);
    }
}