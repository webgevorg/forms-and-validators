<?php

namespace Validator\Rules;

abstract class AbstractRule implements RuleInterface
{
    /**
     * @inheritDoc
     */
    public function message($attribute): string
    {
        return "Attribute {$attribute} must be a valid ". basename(static::class);
    }
}