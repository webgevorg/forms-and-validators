<?php

namespace Validator;

use Exceptions\ValidatorException;
use Traits\ErrorsBag;
use Validator\Rules\RuleInterface;

abstract class AbstractValidator
{
    use ErrorsBag;

    /**
     * Initialise attributes and validate
     * @param array $attributes
     * @param array $values
     * @return AbstractValidator
     * @throws ValidatorException
     */
    public function make(array $attributes, array $values): self
    {
        // loop over attributes and rules array
        foreach ($attributes as $attribute => $rules) {
            // check type of rules
            if (! is_array($rules)) {
                throw new ValidatorException('Rules must be type of array. ' . gettype($rules) . ' given.');
            }
            // check every rule instance and validate
            foreach ($rules as $rule) {

                if (! $rule instanceof RuleInterface) {
                    throw new ValidatorException('Validator Rule must be instance of ' . RuleInterface::class);
                }
                // Validate and add error if rule not pass
                $this->validate($attribute, $values[$attribute], $rule);
            }
        }

        return $this;
    }

    /**
     * Validate attribute
     * @param $attribute
     * @param $value
     * @param RuleInterface $rule
     * @return void
     */
    protected function validate($attribute, $value, RuleInterface $rule): void
    {
        if (! $rule->validate($value)) {
            $this->addError($attribute, $rule->message($attribute));
        }
    }
}