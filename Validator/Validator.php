<?php

namespace Validator;

use Closure;
use Exceptions\ValidatorException;
use Validator\Rules\RuleInterface;

class Validator extends AbstractValidator implements ValidatorEventInterface
{
    /**
     * @inheritDoc
     */
    public function passed(Closure $callback): void
    {
        if (! $this->hasErrors()) {
            $callback();
        }
    }

    /**
     * @inheritDoc
     */
    public function fails(Closure $callback): void
    {
        if ($this->hasErrors()) {
            $callback();
        }
    }
}