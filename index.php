<?php

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($class) {
    include(ROOT . $class . '.php');
});

use Form\LoginForm;
use Validator\Rules\Email;
use Validator\Rules\Numeric;
use Validator\Rules\Required;
use Validator\Rules\InArray;
use Validator\Validator;

$attributes = [
    'name' => 'John',
    'email' => 'johndoe gmail.com',
    'gender' => 'MaLe',
    'age' => '123'
];

$rules = [
    'email' => [new Required, new Email],
    'name' => [new Required],
    'gender' => [new InArray(['male', 'female'], true)],
    'age' => [new Required, new Numeric]
];

/**
 * Creating validator manually
 * @var Validator $validator
 */

// Create validator instance and validate
$validator = (new Validator)->make($rules, $attributes);
// Validator event ON fail
$validator->fails(function () use ($validator) {
    echo '<pre>';
    print_r($validator->getErrors());
    echo '</pre>';
});

// Validator event on success
$validator->passed(function () {
    echo 'Data is valid! <br>';
});

/**
 * Form validator
 * @var LoginForm $loginForm
 */
$loginForm = new LoginForm;
$loginForm->setAttributes($attributes);

$validator = $loginForm->validate();

// Validator event ON fail
$validator->fails(static function () use ($validator) {
    echo '<pre>';
    print_r($validator->getErrors());
    echo '</pre>';
});

// Validator event on success
$validator->passed(static function () {
    echo 'Data is valid! <br>';
});